import { ObjectId } from "mongodb";

export interface Product{
    _id?: ObjectId;
    name: string;
    description:string;
    status: ProductStatus;
    uuid:string;
    tag:string;
    createdAt:number;
    updatedAt:number;
}

export enum ProductStatus{
    Draft="draft",
    Published = "published",
}