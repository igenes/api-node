import { inject } from "inversify";
import { provide } from "inversify-binding-decorators";
import { Collection, Db, Document, MongoClient } from "mongodb";
import { provideSingleton } from "../config/ioc/inversify/CustomProvider";
import { PARAMETERS } from "../config/ioc/parameters";
import { TYPES } from "../config/ioc/types";

export interface IMongoDBConnectionManager {
    connect(): Promise<void>;
    getCollection(name:string):Collection;
}

export class MongoDBConnectionManager implements IMongoDBConnectionManager{
    private readonly client:MongoClient;
    private readonly dbName:string;
    private db:Db;

    constructor(@inject(PARAMETERS.mongodbUrl) url:string,@inject(PARAMETERS.mongodbDatabase) dbName:string ){
        this.client= new MongoClient(url);
        this.dbName = dbName;
    }

    public async connect(): Promise<void> {
        await this.client.connect();
        this.db = this.client.db(this.dbName);
        console.log('MongoDBConnectionManager connected');
    }

    public getCollection(name: string): Collection<Document> {
        return this.db.collection(name);
    }
}