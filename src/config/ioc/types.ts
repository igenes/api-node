export const TYPES = {
    //repository
    ProductRepository:Symbol.for('ProductRepository'),
    //services
    CreateProductService: Symbol.for('CreateProductService'),
    UpdateProductService: Symbol.for('UpdateProductService'),
    DeleteProductService: Symbol.for('DeleteProductService'),

    //utils
    MongodbConnectionManager: Symbol.for('MongodbConnectionManager')
}