import { Product, ProductStatus } from "../../entity/Product";
import { inject } from "inversify";
import { TYPES } from "../../config/ioc/types";
import { IProductRepository } from "../../repository/ProductRepository";
import { provideSingleton } from "../../config/ioc/inversify/CustomProvider";


export interface IDeleteProductService{
    remove(product: Product): Promise<void>;
}
@provideSingleton(TYPES.DeleteProductService)
export class DeleteProductService implements IDeleteProductService{
    constructor(@inject(TYPES.ProductRepository) private readonly productRepository:IProductRepository){}

    public async remove(product: Product): Promise<void> {
        await this.productRepository.remove(product);
    }
}
