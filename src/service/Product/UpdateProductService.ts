import { Product, ProductStatus } from "../../entity/Product";
import { inject } from "inversify";
import { TYPES } from "../../config/ioc/types";
import { IProductRepository } from "../../repository/ProductRepository";
import { provideSingleton } from "../../config/ioc/inversify/CustomProvider";

export interface UpdateProductDto{
    name: string;
    description:string;
    status: ProductStatus;
    tag:string;
}

export interface IUpdateProductService{
    update(product: Product, dto:UpdateProductDto): Promise<Product>;
}
@provideSingleton(TYPES.UpdateProductService)
export class UpdateProductService implements IUpdateProductService{
    constructor(@inject(TYPES.ProductRepository) private readonly productRepository:IProductRepository){}

    public async update(product: Product, {name,description,status,tag}:UpdateProductDto): Promise<Product> {
        const timestamp= (Date.now()/1000 |0)
        product.name = name;
        product.description = description;
        product.status = status;
        product.tag = tag;
        product.updatedAt = timestamp;
        await this.productRepository.persist(product);
        return product;
    }
}
