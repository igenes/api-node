import { Product, ProductStatus } from "../../entity/Product";
import {v4 as uuidv4} from 'uuid';
import { inject } from "inversify";
import { TYPES } from "../../config/ioc/types";
import { IProductRepository } from "../../repository/ProductRepository";
import { provideSingleton } from "../../config/ioc/inversify/CustomProvider";

export interface CreateProductDto{
    name: string;
    description:string;
    status: ProductStatus;
    uuid:string;
    tag:string;
}

export interface ICreateProductService{
    create(dto:CreateProductDto): Promise<Product>;
}

@provideSingleton(TYPES.CreateProductService)
export class CreateProductService implements ICreateProductService{
    constructor(@inject(TYPES.ProductRepository) private readonly repository:IProductRepository){}

    public async create({name,description,status,uuid,tag}:CreateProductDto): Promise<Product> {
        const timestamp= (Date.now()/1000 |0)
        const product: Product = {
            uuid:uuidv4(),
            name,
            description,
            status,
            tag,
            createdAt:timestamp,
            updatedAt:timestamp
        }

        await this.repository.persist(product);
        return product;
    }
}
