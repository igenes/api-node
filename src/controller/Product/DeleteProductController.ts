import { Response, Request} from "express";
import { inject } from "inversify";
import { BaseHttpController, controller, httpDelete, httpPut,request, response } from "inversify-express-utils";
import { TYPES } from "../../config/ioc/types";
import { Product } from "../../entity/Product";
import { IProductRepository } from "../../repository/ProductRepository";
import { IDeleteProductService } from "../../service/Product/DeleteProductService";

@controller('/product')
export class DeleteProductController extends BaseHttpController{
    @inject(TYPES.ProductRepository) private readonly productRepository:IProductRepository;
    @inject(TYPES.DeleteProductService) private readonly deleteService:IDeleteProductService;
    
    @httpDelete('/:uuid')
    public async index(@request() request: Request, @response() response: Response): Promise <Response> {
        const prod :Product|null = await this.productRepository.findOneByUuid(request.params.uuid);

        if(prod == null){
            return response.status(404).send({"error": "Product no found"});
        }
        await this.deleteService.remove(prod);
        return response.send(prod);
    }





}