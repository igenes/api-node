import { BaseHttpController, controller, httpGet,response,request } from "inversify-express-utils";
import { Request, Response } from "express";
import { inject } from "inversify";
import { TYPES } from "../../config/ioc/types";
import { IProductRepository } from "../../repository/ProductRepository";
import { Product } from "../../entity/Product";

@controller('/product')
export class GetProductController extends BaseHttpController{
    @inject(TYPES.ProductRepository) private readonly repository:IProductRepository;


    @httpGet('/')
    public async index(@request() request: Request,@response() response: Response ): Promise <Response>{
        const products:Product[] = await this.repository.findAll();
       return response.json(products);
    }
    
    @httpGet('/:uuid')
    public async getProduct(@request() request: Request, @response() response: Response ):Promise<Response>{
        const product:Product|null = await this.repository.findOneByUuid(request.params.uuid);

        if(product == null){
            return response.status(404).send({"error": "Product no found"});
        }

        return response.json(product);
    }



}