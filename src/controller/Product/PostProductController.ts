import { Request, Response } from "express";
import { inject } from "inversify";
import { BaseHttpController, controller, httpGet, httpPost, request, response } from "inversify-express-utils";
import { TYPES } from "../../config/ioc/types";
import { CreateProductDto, ICreateProductService } from "../../service/Product/CreateProductService";

@controller('/product')
export class PostProductController extends BaseHttpController{
    @inject(TYPES.CreateProductService) private readonly service: ICreateProductService;

    @httpPost("/")
    public async index(@request() request: Request, @response() response: Response ): Promise <Response>{
        const product = await this.service.create(request.body as CreateProductDto)
        return  response.send(product);
    }

}