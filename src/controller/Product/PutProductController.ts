import { Response, Request} from "express";
import { inject } from "inversify";
import { BaseHttpController, controller, httpPut,request, response } from "inversify-express-utils";
import { TYPES } from "../../config/ioc/types";
import { Product } from "../../entity/Product";
import { IProductRepository } from "../../repository/ProductRepository";
import { IUpdateProductService, UpdateProductDto } from "../../service/Product/UpdateProductService";

@controller('/product')
export class PutProductController extends BaseHttpController{
    @inject(TYPES.ProductRepository) private readonly productRepository:IProductRepository;
    @inject(TYPES.UpdateProductService) private readonly updateService:IUpdateProductService
    
    @httpPut('/:uuid')
    public async index(@request() request: Request, @response() response: Response): Promise <Response> {
        const prod :Product|null = await this.productRepository.findOneByUuid(request.params.uuid);

        if(!prod){
            return response.status(404).send({"error": "Product no found"});
        }

        await this.updateService.update(prod ,request.body as UpdateProductDto);
        return response.send(prod);
    }





}