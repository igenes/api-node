import { request } from "express";
import { inject } from "inversify";
import { Collection, Sort } from "mongodb";
import { provideSingleton } from "../config/ioc/inversify/CustomProvider";
import { TYPES } from "../config/ioc/types";
import { Product } from "../entity/Product";
import { IMongoDBConnectionManager } from "../utils/MongoDBConnectionManager";
import { IMongoRepository, MongoRepository } from "../utils/MongoRepository";

export interface IProductRepository extends IMongoRepository{
    findOneByUuid(uuid:string): Promise<Product|null>;
}

const COLLECTION = 'product';

@provideSingleton(TYPES.ProductRepository)
export class ProductRepository  extends MongoRepository implements IProductRepository{
    

    constructor(@inject(TYPES.MongodbConnectionManager) connectionManager:IMongoDBConnectionManager){  
        super();   
        this.collection  = connectionManager.getCollection(COLLECTION);
    }
    
    public async findOneByUuid(uuid: string): Promise <Product|null> { 
        return await this.findOneBy({uuid});
    }
}