import fs from "fs";
import { bootstrap } from "./config/bootstrap";
import path from "path";
import dotenv from "dotenv";


const run = async (): Promise<void>  => {

    if(!fs.existsSync(path.join(__dirname,"/../.env"))){
        process.exit(1);
    }
    dotenv.config({path: path.resolve(__dirname + "/../.env")});    
    const app = await bootstrap();
    
    app.listen(process.env.HTTP_PORT, ()=>{
        console.log("server is running in the port: "+ process.env.HTTP_PORT);
    });
};

run();